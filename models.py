import datetime as dt

from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class DictModel(db.Model):
    __abstract__ = True

    excluded = set()

    def as_dict(self):
        return {
            c: getattr(self, c)
            for c in set(self.__table__.columns.keys()).difference(self.excluded)}

    def update(self, update_dict):
        for key, val in update_dict.items():
            setattr(self, key, val)


class Contact(DictModel):

    excluded = {'created'}

    username = db.Column(db.String(80), unique=True, nullable=False, primary_key=True)
    first_name = db.Column(db.String(40), unique=False, nullable=False)
    last_name = db.Column(db.String(40), unique=False, nullable=False)
    created = db.Column(db.DateTime, default=dt.datetime.utcnow)

    def __repr__(self):
        return "<User {}>".format(self.username)


class Email(DictModel):
    email = db.Column(db.String(80), unique=True, nullable=False, primary_key=True)

    contact_username = db.Column(db.Integer, db.ForeignKey('contact.username'),
        nullable=False)
    contact = db.relationship('Contact', backref=db.backref('emails'))
