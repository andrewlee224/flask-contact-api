To test, install pytest and run

$ pytest

in the project's root directory


To run celery and celery-beat, issue these commands:
$ celery -A celery_worker:celery worker --loglevel=DEBUG
$ celery -A celery_worker:celery beat --loglevel=INFO

The application assumes that redis is running on default port 6379