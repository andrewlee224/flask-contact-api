celery==4.3.0
Flask==1.0.3
Flask-RESTful==0.3.7
Flask-SQLAlchemy==2.4.0
SQLAlchemy==1.3.4
pytest==4.6.3
