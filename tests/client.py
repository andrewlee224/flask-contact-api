import os

import pytest

import userapp
from models import db


@pytest.fixture
def client():
    db_fd = open("tests/test.db", 'w')
    userapp.app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///tests/test.db"
    userapp.app.config['TESTING'] = True
    client = userapp.app.test_client()

    with userapp.app.app_context():
        db.init_app(userapp.app)
        db.create_all()

    yield client

    db_fd.close()
    os.unlink("tests/test.db")
