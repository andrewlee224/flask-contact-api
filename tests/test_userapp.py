from tests.client import client


def test_empty_contacts(client):
    """Start with a blank database."""

    resp = client.get('/contacts')
    assert resp.json == []


def test_creating_contacts(client):
    resp = client.post(
        '/contact',
        data={
            'username': 'andrzej',
            'email': ['andrzej@a.com', 'andrzej@b.com'],
            'first_name': 'Andrzej',
            'last_name': 'Lichaczewski',
        })

    assert resp.status_code == 200

    resp_all = client.get('/contacts')
    assert len(resp_all.json) == 1


def test_search(client):
    resp = client.post(
        '/contact',
        data={
            'username': 'andrzej',
            'email': ['andrzej@a.com', 'andrzej@b.com'],
            'first_name': 'Andrzej',
            'last_name': 'Lichaczewski',
        })

    assert resp.status_code == 200

    resp_search = client.get('/contacts/search?email=andrzej@b.com')

    assert resp_search.json['username'] == 'andrzej'


def test_updating_contacts(client):
    resp = client.post(
        '/contact',
        data={
            'username': 'andrzej',
            'email': ['andrzej@a.com', 'andrzej@b.com'],
            'first_name': 'Andrzej',
            'last_name': 'Lichaczewski',
        })

    assert resp.status_code == 200

    resp = client.put(
        '/contact/andrzej',
        data={
            'email': ['andrzej@c.com', 'andrzej@d.com'],
            'first_name': 'Andrzej',
            'last_name': 'Lichaczewski',
        })

    assert resp.status_code == 200

    resp_search = client.get('/contacts/search?email=andrzej@c.com')

    assert resp_search.json['username'] == 'andrzej'


def test_deleting_contacts(client):
    resp = client.post(
        '/contact',
        data={
            'username': 'andrzej',
            'email': ['andrzej@a.com', 'andrzej@b.com'],
            'first_name': 'Andrzej',
            'last_name': 'Lichaczewski',
        })

    assert resp.status_code == 200

    resp_all = client.get('/contacts')
    assert len(resp_all.json) == 1

    resp_delete = client.delete('/contact/andrzej')
    assert resp_delete.status_code == 200

    resp_all = client.get('/contacts')
    assert len(resp_all.json) == 1
