import random
import datetime as dt

from flask import Flask, request
from flask_restful import Api, Resource, reqparse, abort
from sqlalchemy.exc import SQLAlchemyError

import models
from models import db

from tasks import make_celery


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///users.db'

api = Api(app)

with app.app_context():
    db.init_app(app)
    db.create_all()

app.config.update(
    CELERY_BROKER_URL='redis://localhost:6379',
    CELERY_RESULT_BACKEND='redis://localhost:6379'
)
celery = make_celery(app)



@celery.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    # Calls reverse_messages every 10 seconds.
    sender.add_periodic_task(15.0, create_contact, name='create contact every 15s')


@celery.task()
def create_contact():
    contact = models.Contact(
        username='user_{}'.format(random.randint(0, 1e6)),
        first_name='first_name',
        last_name='last_name'
    )

    for _ in range(2):
        email_instance = models.Email(email='email_{}@gmail.com'.format(random.randint(0, 1e6)))
        contact.emails.append(email_instance)

    db.session.add(contact)
    db.session.commit()

    dt_cutoff = dt.datetime.utcnow() - dt.timedelta(minutes=1)
    contacts_to_delete = models.Contact.query.filter(models.Contact.created <= dt_cutoff)

    for c in contacts_to_delete:
        emails = c.emails
        for email in emails:
            db.session.delete(email)
        db.session.delete(c)
    db.session.commit()


contact_parser = reqparse.RequestParser()
contact_parser.add_argument('username')
contact_parser.add_argument('email', action='append')
contact_parser.add_argument('first_name')
contact_parser.add_argument('last_name')


def get_contact_by_username(username):
    contact = models.Contact.query.filter_by(username=username).first()

    if not contact:
        abort(404, message="Contact with username '{}' does not exist".format(username))

    return contact


class Contact(Resource):
    def get(self, username):
        contact = get_contact_by_username(username)

        contact_dict = contact.as_dict()

        emails = []
        for email in contact.emails:
            emails.append(email.email)
        contact_dict['email'] = emails

        return contact_dict

    def post(self):
        args = contact_parser.parse_args()
        contact = models.Contact(
            username=args['username'],
            first_name=args['first_name'],
            last_name=args['last_name']
        )

        for email in args['email']:
            email_instance = models.Email(email=email)
            contact.emails.append(email_instance)

        db.session.add(contact)

        try:
            db.session.commit()
        except SQLAlchemyError:
            abort(400, message="Bad request data - missing or malformed fields")

    def put(self, username):
        args = contact_parser.parse_args()
        args['username'] = username
        emails = args.pop('email')

        contact = get_contact_by_username(username)
        args['created'] = contact.created

        contact.update(args)
        for email in contact.emails:
            db.session.delete(email)

        for email in emails:
            email_instance = models.Email(email=email)
            contact.emails.append(email_instance)

        try:
            db.session.commit()
        except SQLAlchemyError:
            abort(400, message="Bad request data - missing or malformed fields")

    def delete(self, username):
        contact = get_contact_by_username(username)

        try:
            emails = contact.emails
            db.session.delete(contact)
            for email in emails:
                db.session.delete(email)
        except SQLAlchemyError:
            abort(400, message="Could not delete contact instance")


class ContactSearch(Resource):

    def get(self):
        args = request.args
        email = args['email']
        email_instance = models.Email.query.filter_by(email=email).first()

        if not email_instance:
            abort(404, message="Email '{}' does not exist".format(email))

        contact = email_instance.contact

        if not contact:
            abort(404, message="Contact with email '{}' does not exist".format(email))

        return contact.as_dict()


class ContactList(Resource):

    def get(self):
        contacts = [c.as_dict() for c in models.Contact.query.all()]

        return contacts



api.add_resource(Contact, '/contact', '/contact/<username>')
api.add_resource(ContactList, '/contacts')
api.add_resource(ContactSearch, '/contacts/search')


if __name__ == '__main__':
    app.run(debug=True)
